## **Product Name #1**

DermLens

**Product Link**

[Link](https://aws.amazon.com/deeplens/community-projects/DermLens/)

**Product Short Description**

DermLens aims to assist patients to monitor and manage a skin condition called psoriasis. Using DermLens, we can deliver a cost-effective, scalable solution to change the lives of millions by empowering patients with psoriasis to monitor and manage their condition. Running machine learning algorithms and inference locally in real time is a game changer, allowing classification and segmentation on the edge of the network at the location of the patient, rather than depend on high bandwidth connectivity to centralized hardware.

**Product is combination of features**

1. Image segmentation
2. Image classification 

**Product is provided by which company?**

Amazon

## **Product Name #2**

IBM Crypto Anchor Verifier

**Product Link**

[Link](https://www.ibm.com/products/verifier)

**Product Short Description**

IBM Crypto Anchor Verifier, brings together innovations in AI and optical imaging to help prove the identity, authenticity or quality of products or substances in the field.

**Product is combination of features**

1. Object detection

**Product is provided by which company?**

 IBM

